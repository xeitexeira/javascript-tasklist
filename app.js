// DEFINE THE UI VARIABLES
const form = document.querySelector('#task-form');
const taskList = document.querySelector('.collection');
const clrBtn = document.querySelector('.clear-tasks');
const filter = document.querySelector('#filter');
const taskInput = document.querySelector('#task');

// LOAD ALL EVENT LISTENERS
loadEventListeners();

function loadEventListeners() {

  // DOM Load Event (Get All the tasks from LS)
  document.addEventListener('DOMContentLoaded', getTasks);
  // ADD TASK
  form.addEventListener('submit', addTask);
  // REMOVE TASK
  taskList.addEventListener('click', removeTask);
  // CLEAR TASKS
  clrBtn.addEventListener('click', clearTasks);
  // FILTER TASKS
  filter.addEventListener('keyup', filterTasks);

}
function getTasks() {
  let tasks;
  if(localStorage.getItem('tasks') === null) {
    tasks = [];
  } else {
    tasks = JSON.parse(localStorage.getItem('tasks'));
  }
  tasks.forEach(function (task) {
    // CREATE AN LI FOR CONTAINER OF EACH TASK
    const li = document.createElement('li');
    li.className = 'collection-item';

    // Create a text node w/ class and append to Li
    li.appendChild(document.createTextNode(task));
    const link = document.createElement('a');
    link.className = 'delete-item secondary-content';
    link.innerHTML = '<i class="fa fa-remove"></i>';
    li.appendChild(link);

    // APPEND LI TO UL (call the parent before the child)
    taskList.appendChild(li);
  })


  localStorage.setItem('tasks', JSON.stringify(tasks));
}

function addTask(e) {
  if(taskInput.value === '') {
    alert('Add a task');
  }

  // CREATE LI AND ADD CLASS
  const li = document.createElement('li');
  li.className = 'collection-item';

  // Create a text node w/ class and append to Li
  li.appendChild(document.createTextNode(taskInput.value));
  const link = document.createElement('a');
  link.className = 'delete-item secondary-content';
  link.innerHTML = '<i class="fa fa-remove"></i>';
  li.appendChild(link);

  // APPEND LI TO UL (call the parent before the child)
  taskList.appendChild(li);
  
  // STORE IN LOCAL STORAGE
  storeTaskInLocalStorage(taskInput.value);


  // CLEAR INPUT
  taskInput.value = '';
  e.preventDefault();
}

// STORING A TASK IN LOCAL STORAGE
function storeTaskInLocalStorage(task) {
  let tasks;
  if(localStorage.getItem('tasks') === null) {
    tasks = [];
  } else {
    // SHOULD PARSE THE OBJECT BECAUSE IT WAS ARRAY WHEN STORED IN THE LOCAL STORAGE (GETTING THE ARRAY)
    tasks = JSON.parse(localStorage.getItem('tasks'));
  }
  // ADDING A TASK TO THE ARRAY
  tasks.push(task);
  
  // STORE THE TASK ARRAY (W/ THE NEW TASK) BACK TO THE LS
  localStorage.setItem('tasks', JSON.stringify(tasks));
}

function removeTask(e) {
  if (e.target.parentElement.classList.contains('delete-item')) {

    // FOR CONFIRMATION OF DELETE
    if(confirm('Are You Sure?')){
    e.target.parentElement.parentElement.remove();

    // REMOVE TASK FROM LOCAL STORAGE
    removeTaskFromLocalStorage(e.target.parentElement.parentElement)
    } 
  }
}

// REMOVE A TASK FROM LOCAL STORAGE
function removeTaskFromLocalStorage(taskItem) {
  let tasks;
  if(localStorage.getItem('tasks') === null) {
    tasks = [];
  } else {
    tasks = JSON.parse(localStorage.getItem('tasks'));
  }
  // LOOP THROUGH THE ARRAY IN THE LS TO CHECK IF TASK IS EXISTING
  tasks.forEach(function (task, index) {
    if(taskItem.textContent === task) {
      tasks.splice(index, 1);
    } 
  })
  localStorage.setItem('tasks', JSON.stringify(tasks));
}

function clearTasks() {
  // taskList.innerHTML = '';

  // SAME FUNCTION BUT FASTER
  if(confirm('Are You Sure?')){
    while (taskList.firstChild) {
      taskList.removeChild(taskList.firstChild);
    }
  }

  // CLEAR TASKS FROM LOCAL STORAGE
  clearTaskFromLocalStorage();
}

function clearTaskFromLocalStorage() {
  localStorage.clear();
}

function filterTasks(e) {
  const text = e.target.value.toLowerCase();

  // GETS THE TEXT CONTENT OF THE LI'S FOR 
  document.querySelectorAll('.collection-item').forEach(function(task) { 
    const item = task.firstChild.textContent;
    if (item.toLowerCase().indexOf(text) != -1) {
      task.style.display = 'block';
    } else {
      task.style.display = 'none';
    }
    // console.log(item);
  });
}